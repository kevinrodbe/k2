/* Dependencias */
var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	concat = require('gulp-concat'),
	stylus = require('gulp-stylus'),
	nib = require('nib'),
	watch = require('gulp-watch'),
	uglify = require('gulp-uglify'),
	minifyCSS = require('gulp-minify-css'),
	gutil = require('gulp-util'),
	imagemin    = require('gulp-imagemin'),
	jpegoptim = require('imagemin-jpegoptim'),
	pngquant = require('imagemin-pngquant'),
	Filter = require('gulp-filter'), //Concatenar stylus al css
	newer = require('gulp-newer');
	//browserSync = require('browser-sync');
/* Configuración global */
var path={
	inputStylus:'./desarrollo/css/*.styl',
	inputCss:'./desarrollo/css/*.css',
	nameCss:'style.min.css',
	outputStylus:'./desarrollo/css/',
	outputCss:'./produccion/css/',

	inputJsGeneral:'desarrollo/js/general/*.js',
	nameJsGeneral:'jsgeneral.min.js',
	outputJsGeneral:'./produccion/js/general/',

	inputJsLibreria:'./desarrollo/js/lib/**',
	outputJsLibreria:'./produccion/js/lib/',

	inputImg:'desarrollo/img/*.{gif,png,jpg,svg,jpeg}',
	outputImg:'./produccion/img/'
};
gulp.task('nib', function(){
    gulp.src(path.inputStylus)
        .pipe(stylus({ use: [nib()], compress: true }))
        .pipe(gulp.dest(path.outputStylus));
});
/* compilando stylus, concatenando y minificando todos los css */
gulp.task('css', function () {
	var filter=Filter('**/*.styl');
	gulp.src([path.inputStylus,path.inputCss])
		.pipe(newer(path.outputCss))
		.pipe(filter)
		.pipe(stylus({ use: [nib()], compress: true }))
		.pipe(filter.restore())
		.pipe(concat(path.nameCss))
		.pipe(minifyCSS())
		.pipe(gulp.dest(path.outputCss));
});
/* Configuración de la tarea 'demo' */
gulp.task('jsgeneral', function () {
	gulp.src(path.inputJsGeneral)
		//.pipe(jshint())
		.pipe(concat(path.nameJsGeneral))
		.pipe(uglify())
		.pipe(gulp.dest(path.outputJsGeneral))
});
/* Copiar las libs y utilitarios a produccion */
gulp.task('copyLibreria', function() {
	gulp.src(path.inputJsLibreria).pipe(gulp.dest(path.outputJsLibreria));
});
/* optimizar img jpg,svg y png */
gulp.task('img', function() {
    gulp.src(path.inputImg)
		.pipe(newer(path.outputImg))
		.pipe(imagemin({ 
			progressive: true ,
			interlaced: true, 
			svgoPlugins: [{removeViewBox: false}],
			use: [
				pngquant({quality: '60-70', speed: 4})
				//jpegoptim({progressive: true,max: 60})
			]
		}))
		.pipe(gulp.dest(path.outputImg));
});
gulp.task('ver', function () {
	/* watch para stylus */
	gulp.watch(path.inputStylus, ['css']);
	/* watch para js */
	gulp.watch(path.inputJsGeneral, ['jsgeneral']);
	/* watch para js libreria */
	gulp.watch(path.inputJsLibreria, ['copyLibreria']);
	/* watch para img */
	gulp.watch(path.inputImg, ['img']);
});
