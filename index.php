<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="es"> <!--<![endif]-->
<head>
<title></title>
<?php include('include/head.html'); ?>
</head>
<body>
	<?php include('include/menu.html'); ?>
	<section class="mapaWrapper">
		<div id="mapa"></div>
		<div class="presentacionWrapper" style="background-image:url('produccion/img/casa1.jpg');">
			<div class="contenedor">
				<div class="textoBox">
					<h1><span>Vende</span> tu casa con <span>seguridad</span></h1>
					<div class="texto">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nesciunt quaerat, reprehenderit tempora molestias distinctio dolore impedit beatae voluptas sint.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="propiedadesRecientes">
		<div class="contenedor cleaner">
			<div class="propiedadesWrapper">
				<div class="tituloBox">
					<div class="tituloTexto">
						<h2><span>Propiedades</span> recientes</h2>
					</div>
				</div>
				<ul class="propiedadList">
					<li class="propiedadItem">
						<div class="propiedadBox">
							<a href="" class="propiedadLink">
								<figure class="imgBox">
									<img src="produccion/img/casa1.jpg" alt="">
									<div class="overlayBox">
										<span class="ico">M</span>
									</div>
								</figure>
								<div class="nuevo">Nuevo</div>
								<div class="contenidoBox">
									<div class="nameBox">
										<h3>Aliquam tincidunt</h3>
									</div>
									<ul class="iconoList">
										<li class="iconoItem"><span class="ico">M</span><span class="numero">3</span></li>
										<li class="iconoItem"><span class="ico">M</span><span class="numero">123</span></li>
									</ul>
									<div class="descripcion">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, alias modi facere.</p>
									</div>
								</div>
							</a>
							<div class="precioBox cleaner">
								<div class="precio"><p>$ 150 /sq. ft. </p></div>
								<a href="" class="share"><spam class="ico">M</spam></a>
							</div>
						</div>
					</li>
					<li class="propiedadItem">
						<div class="propiedadBox">
							<a href="" class="propiedadLink">
								<figure class="imgBox">
									<img src="produccion/img/casa1.jpg" alt="">
									<div class="overlayBox">
										<span class="ico">M</span>
									</div>
								</figure>
								<div class="nuevo">Nuevo</div>
								<div class="contenidoBox">
									<div class="nameBox">
										<h3>Aliquam tincidunt</h3>
									</div>
									<ul class="iconoList">
										<li class="iconoItem"><span class="ico">M</span><span class="numero">3</span></li>
										<li class="iconoItem"><span class="ico">M</span><span class="numero">123</span></li>
									</ul>
									<div class="descripcion">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, alias modi facere.</p>
									</div>
								</div>
							</a>
							<div class="precioBox cleaner">
								<div class="precio"><p>$ 150 /sq. ft. </p></div>
								<a href="" class="share"><spam class="ico">M</spam></a>
							</div>
						</div>
					</li>
					<li class="propiedadItem">
						<div class="propiedadBox">
							<a href="" class="propiedadLink">
								<figure class="imgBox">
									<img src="produccion/img/casa1.jpg" alt="">
									<div class="overlayBox">
										<span class="ico">M</span>
									</div>
								</figure>
								<div class="nuevo">Nuevo</div>
								<div class="contenidoBox">
									<div class="nameBox">
										<h3>Aliquam tincidunt</h3>
									</div>
									<ul class="iconoList">
										<li class="iconoItem"><span class="ico">M</span><span class="numero">3</span></li>
										<li class="iconoItem"><span class="ico">M</span><span class="numero">123</span></li>
									</ul>
									<div class="descripcion">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, alias modi facere.</p>
									</div>
								</div>
							</a>
							<div class="precioBox cleaner">
								<div class="precio"><p>$ 150 /sq. ft. </p></div>
								<a href="" class="share"><spam class="ico">M</spam></a>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<aside class="enlacesWrapper">
				<div class="tituloBox">
					<div class="tituloTexto">
						<h2>Enlaces</h2>
					</div>
				</div>
				<ul class="enlaceList">
					<li class="enlaceItem">
						<div class="enlaceBox">
							<div class="titulo"><h3>Estudio ASD</h3></div>
							<div class="texto"><p>Abogados asociados de Perú</p></div>
							<a href="http:/www.google.com.pe" target="_blank" class="web">www.abogados.pe</a>
						</div>
					</li>
				</ul>
			</aside>
		</div>
	</section>
	<?php include('include/js-general.html'); ?>
	<script src='https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js'></script>
<script> 
	google.maps.event.addDomListener(window, 'load', init);
	var map;
	function init() {
		var mapOptions = {
			center: new google.maps.LatLng(-12.126775, -77.032114),
			zoom: 15,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.DEFAULT,
			},
			disableDoubleClickZoom: false,
			mapTypeControl: true,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			},
			scaleControl: true,
			scrollwheel: false,
			panControl: true,
			streetViewControl: true,
			draggable : true,
			overviewMapControl: false,
			overviewMapControlOptions: {
				opened: false,
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [
				{
					"featureType": "landscape",
					"stylers": [
						{
							"hue": "#FFA800"
						},
						{
							"saturation": 0
						},
						{
							"lightness": 0
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "road.highway",
					"stylers": [
						{
							"hue": "#53FF00"
						},
						{
							"saturation": -73
						},
						{
							"lightness": 40
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "road.arterial",
					"stylers": [
						{
							"hue": "#FBFF00"
						},
						{
							"saturation": 0
						},
						{
							"lightness": 0
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "road.local",
					"stylers": [
						{
							"hue": "#00FFFD"
						},
						{
							"saturation": 0
						},
						{
							"lightness": 30
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "water",
					"stylers": [
						{
							"hue": "#00BFFF"
						},
						{
							"saturation": 6
						},
						{
							"lightness": 8
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "poi",
					"stylers": [
						{
							"hue": "#679714"
						},
						{
							"saturation": 33.4
						},
						{
							"lightness": -25.4
						},
						{
							"gamma": 1
						}
					]
				}
			],
		}
		var mapElement = document.getElementById('mapa');
		var map = new google.maps.Map(mapElement, mapOptions);
		var locations = [
			['Mundo Vacaciones!', 'Disfruta del mundo', 'undefined', 'undefined', 'undefined', -12.126775, -77.032114, 'https://mapbuildr.com/assets/img/markers/solid-pin-red.png']
		];
		for (i = 0; i < locations.length; i++) {
			if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
			if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
			if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
			if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
			if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
			marker = new google.maps.Marker({
				icon: markericon,
				position: new google.maps.LatLng(locations[i][5], locations[i][6]),
				map: map,
				title: locations[i][0],
				desc: description,
				tel: telephone,
				email: email,
				web: web,
				animation: google.maps.Animation.DROP
			});
			if (web.substring(0, 7) != "http://") {
				link = "http://" + web;
			} else {
				link = web;
			}
			bindInfoWindow(marker, map, locations[i][0], description, telephone, email, web, link);
		}
		function bindInfoWindow(marker, map, title, desc, telephone, email, web, link) {
			var infoWindowVisible = (function () {
				var currentlyVisible = false;
				return function (visible) {
					if (visible !== undefined) {
						currentlyVisible = visible;
					}
					return currentlyVisible;
				};
			}());
			iw = new google.maps.InfoWindow();
			google.maps.event.addListener(marker, 'click', function() {
				if (infoWindowVisible()) {
					iw.close();
					infoWindowVisible(false);
				} else {
					var html= "<div style='color:#000;background-color:#fff;padding:5px;width:150px;'><h4>"+title+"</h4><p>"+desc+"<p><a href='"+link+"'' >"+web+"<a></div>";
					iw = new google.maps.InfoWindow({content:html});
					iw.open(map,marker);
					infoWindowVisible(true);
				}
			});
			google.maps.event.addListener(iw, 'closeclick', function () {
				infoWindowVisible(false);
			});
		}
	}
</script>
</body>
</html>